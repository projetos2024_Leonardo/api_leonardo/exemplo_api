const cron = require("node-cron");
const cleanUpSchedules = require ("./services/cleanUpSchedulesServices");

// Agendamento da limpeza
cron.schedule("*/30 * * * * *", async()=>{
    try{
        await cleanUpSchedules();
        console.log("limpeza automatica executada");
    }catch(errror){
        console.error("Erro ao executar limpeza");
    }
})